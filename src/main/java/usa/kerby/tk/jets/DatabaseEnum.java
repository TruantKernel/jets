package usa.kerby.tk.jets;

import java.util.Arrays;

/**
 * @author Trevor Kerby
 * @since Apr 26, 2021
 */
public interface DatabaseEnum<E extends Enum<E>> {

	String getNaturalKey();

	public static <E extends Enum<E> & DatabaseEnum<?>> E selectValueOfNaturalKey(Class<E> databaseEnumClass, String name) {
		return Arrays.stream(databaseEnumClass.getEnumConstants()).filter(m -> (m).getNaturalKey().equals(name)).findAny().orElse(null);
	}
}
