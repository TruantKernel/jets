package usa.kerby.tk.jets;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

/**
 * @author Trevor Kerby
 * @since Apr 23, 2021
 */

public class EnumTableIndexBuilder<T> {

	private final Map<DataSource, Set<EnumTableSynchronizer<T>>> synchronizers;

	public EnumTableIndexBuilder() {
		this.synchronizers = new HashMap<>();
	}

	@SuppressWarnings("unchecked")
	public EnumTableIndexBuilder<T> withSynchronizer(DataSource source, EnumTableSynchronizer<T> synchronizer) {
		return this.withSynchronizers(source, synchronizer);
	}

	@SuppressWarnings("unchecked")
	public EnumTableIndexBuilder<T> withSynchronizers(DataSource source, EnumTableSynchronizer<T>... synchronizers) {
		final Set<EnumTableSynchronizer<T>> synchronizersOfSource = findOrMakeSychronizerSet(source);
		for (EnumTableSynchronizer<T> synchronizer : synchronizers) {
			synchronizersOfSource.add(synchronizer);
		}
		return this;
	}

	public EnumKeyIndex<T> build() {
		final Map<String, Map<String, T>> masterIndex = new HashMap<String, Map<String, T>>();
		for (DataSource source : this.synchronizers.keySet()) {
			final Connection con = this.makeConnection(source);
			Map<String, Map<String, T>> index = this.indexEnumTables(this.synchronizers.get(source), con);
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			masterIndex.putAll(index);
		}
		return new EnumKeyIndex<T>(masterIndex);
	}

	public EnumKeyIndex<T> buildAndDispose() {
		final EnumKeyIndex<T> index = this.build();
		this.disposeSynchronizers();
		return index;
	}

	public void disposeSynchronizers() {
		this.synchronizers.clear();
	}

	private Connection makeConnection(DataSource source) {
		Connection con = null;
		if (source == null) {
			new RuntimeException("supplied DataSource is null");
		}
		try {
			con = source.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	private Set<EnumTableSynchronizer<T>> findOrMakeSychronizerSet(DataSource source) {
		Set<EnumTableSynchronizer<T>> synchronizersOfSource = this.synchronizers.get(source);
		if (synchronizersOfSource == null) {
			synchronizersOfSource = new HashSet<>();
			this.synchronizers.put(source, synchronizersOfSource);
		}
		return synchronizersOfSource;
	}

	private Map<String, Map<String, T>> indexEnumTables(Set<EnumTableSynchronizer<T>> synchronizers, Connection connection) {
		final Map<String, Map<String, T>> index = new HashMap<String, Map<String, T>>();
		for (EnumTableSynchronizer<T> synchronizer : synchronizers) {
			try {
				synchronizer.sync(connection);
				index.put(synchronizer.getEnumName(), Collections.unmodifiableMap(synchronizer.retrieveKeys(connection)));
			} catch (EnumTableSQLException e) {
				e.printStackTrace();
			}
		}
		return index;
	}
}
