package usa.kerby.tk.jets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Trevor Kerby
 * @since Apr 16, 2021
 */
public abstract class AbstractEnumTableSynchronizer<E extends Enum<E>, T> implements EnumTableSynchronizer<T> {

	private static String DEFAULT_NAME_COLUMN = "name";
	private static String DEFAULT_ID_POSTFIX = "_id";

	private final Class<E> enumClass;
	private final String table;
	private final String idColumn;
	private final String nameColumn;

	protected AbstractEnumTableSynchronizer(Class<E> enumClass, String table) {
		this(enumClass, table, table + DEFAULT_ID_POSTFIX, DEFAULT_NAME_COLUMN);
	}

	protected AbstractEnumTableSynchronizer(Class<E> enumClass, String table, String idColumn) {
		this(enumClass, table, idColumn, DEFAULT_NAME_COLUMN);
	}

	protected AbstractEnumTableSynchronizer(Class<E> enumClass, String table, String idColumn, String nameColumn) {
		this.nameColumn = nameColumn;
		this.idColumn = idColumn;
		this.enumClass = enumClass;
		this.table = table;
	}

	public String getEnumName() {
		return this.enumClass.getSimpleName();
	}

	protected E[] getConstants() {
		return this.enumClass.getEnumConstants();
	}

	public void sync(Connection conn) throws EnumTableSQLException {
		try {
			PreparedStatement statement = prepareSyncStatement(conn);
			for (E constant : this.getConstants()) {
				this.processConstant(statement, constant);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new EnumTableSQLException(this.getEnumName(), e);
		}
	}

	public Map<String, T> retrieveKeys(Connection conn) throws EnumTableSQLException {
		Map<String, T> enumIds = new HashMap<>();
		final String sql = "SELECT " + this.idColumn + "," + this.nameColumn + " FROM " + this.table;
		try {
			ResultSet res = conn.prepareStatement(sql).executeQuery();
			while (res.next()) {
				final T id = convertIdDatabaseValue(res, idColumn);
				final String enumConstantString = res.getString(this.nameColumn);
				System.out.println(enumConstantString + " : " + id.toString());
				enumIds.put(enumConstantString, id);
			}
		} catch (SQLException e) {
			throw new EnumTableSQLException(this.getEnumName(), e);
		}
		return Collections.unmodifiableMap(enumIds);
	}

	protected abstract T convertIdDatabaseValue(ResultSet result, String idColumnName);

	protected abstract PreparedStatement prepareSyncStatement(Connection conn) throws SQLException;

	protected abstract void processConstant(PreparedStatement statement, E constant) throws SQLException;

}
