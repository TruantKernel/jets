package usa.kerby.tk.jets;

import java.sql.SQLException;

public class EnumTableSQLException extends SQLException {
	private static final long serialVersionUID = 1L;

	public EnumTableSQLException(String enumName, SQLException exception) {
		super("Failed to synchronize enum table: " + enumName, exception.getSQLState(), exception);
	}

}
