package usa.kerby.tk.jets;

import java.sql.Connection;
import java.util.Map;

/**
 * @author Trevor Kerby
 * @since Apr 16, 2021
 */
public interface EnumTableSynchronizer<T> {

	public void sync(Connection conn) throws EnumTableSQLException;

	public Map<String, T> retrieveKeys(Connection conn) throws EnumTableSQLException;
	
	public String getEnumName();
}
