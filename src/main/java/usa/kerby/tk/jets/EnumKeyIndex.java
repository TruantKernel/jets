package usa.kerby.tk.jets;

import java.util.Collections;
import java.util.Map;

/**
 * @author Trevor Kerby
 * @since Aug 10, 2021
 */
public class EnumKeyIndex<T> {

	private final Map<String, Map<String, T>> index;

	public EnumKeyIndex(Map<String, Map<String, T>> map) {
		this.index = Collections.unmodifiableMap(map);
	}

	public Map<String, T> get(String key) {
		return this.index.get(key);
	}

	public int size() {
		return this.index.size();
	}

}
