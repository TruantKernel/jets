package usa.kerby.tk.jets;

import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import jakarta.persistence.AttributeConverter;

/**
 * @author Trevor Kerby
 * @since Apr 23, 2021
 */

public abstract class EnumTableConstantConverter<T extends Enum<T> & DatabaseEnum<T>, K>
		implements AttributeConverter<T, K> {

	@Inject
	private EnumKeyIndex<K> index;

	protected final Class<T> enumClass;

	protected abstract T[] getEnumValues();

	protected EnumTableConstantConverter(Class<T> enumClass) {
		this.enumClass = enumClass;
	}

	private Map<String, K> getEnumKeys() {
		return this.index.get(this.enumClass.getSimpleName());
	}

	@Override
	public K convertToDatabaseColumn(T attribute) {
		return this.getEnumKeys().get(attribute.getNaturalKey());
	}

	@Override
	public T convertToEntityAttribute(K dbData) {
		for (Entry<String, K> entry : this.getEnumKeys().entrySet()) {
			if (entry.getValue().equals(dbData)) {
				return DatabaseEnum.selectValueOfNaturalKey(enumClass, entry.getKey());
			}
		}
		new Exception("no corresponding enum value");
		return null;
	}

}
