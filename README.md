# Java Enum Table Synchronizer
`jets` synchronizes the values of a specially defined `enum`, `DatabaseEnum`,  to an `sql` table by adding non-existing rows to the associated table, fetching the `primary key` of each row in the table, and added mappings between each `enum` value and it respective `primary key` to an `EnumKeyIndex`. This `EnumKeyIndex` is injected via CDI into a `EnumTableConstantConverter`, an implementation of `javax.persistence.AttributeConverter`, to convert references of a `DatabaseEnum` being persisted into their associated `primary key` and vice-versa upon retrieval.

# Usage

Define a `DatabaseEnum`

```java
public enum FileExtension implements DatabaseEnum<FileExtension>{

	PNG("png"), JPEG("jpeg"), MP4("mp4"), WEBM("webm"), OOG("oog");

	private final String name;

	FileExtension(String name) {
		this.name = name;
	}

	@Override
	public String getNaturalId() {
		return this.name;
	}
}
```

Define an implementation of `EnumTableSynchronizer`, such as an `UUIDIndexedEnumTableSynchronizer` for tables with a UUID as its primary key

```java
public class ExtensionEnumTableSynchronizer extends UUIDIndexedEnumTableSynchronizer<FileExtension> {

	private final static String TABLE_NAME = "extension";

	/**
	 * @param enumClass
	 */
	public ExtensionEnumTableSynchronizer() {
		super(FileExtension.class, TABLE_NAME);
	}

	@Override
	protected PreparedStatement prepareSyncStatement(Connection conn) throws SQLException {
		return conn.prepareStatement("INSERT IGNORE INTO extension (extension_id,name) VALUES (?,?)");
	}

	@Override
	protected void processConstant(PreparedStatement statement, FileExtension constant) throws SQLException {
		statement.setObject(1, generateId());
		statement.setString(2, constant.getNaturalId());
	}

}
```

Define a class that extends `EnumTableConstantConverter<FileExtension, UUID>`

```java
@Converter(autoApply = true)
public class FileExtensionConverter extends EnumTableConstantConverter<FileExtension, UUID> {

	/**
	 * @param enumClass
	 */
	protected FileExtensionConverter() {
		super(FileExtension.class);
	}

	@Override
	protected FileExtension[] getEnumValues() {
		return FileExtension.values();
	}

}
```

Define an injectable class that extends `AbstractEnumTableManager` and can provide the `EnumKeyIndex` to the CDI injection point in `EnumTableConstantConverter`. This class should instantiate the `EnumTableIndexBuilder` and use it to register Synchronizers with the `withSynchronizer(DataSource source, EnumTableSynchronizer<T> synchronizer)` method or the `withSynchronizers(DataSource source, EnumTableSynchronizer<T>... synchronizers)` method, passing in the appropriate `DataSource` that contains the respective table.

```java
@Singleton
@Startup
public class ApplicationEnumTableManager extends AbstractEnumTableManager {

	@SuppressWarnings("unchecked")
	public ApplicationEnumTableManager() {
		initializeContext("openejb:Resource");
		EnumTableIndexBuilder<UUID> builder = new EnumTableIndexBuilder<UUID>();
		builder.withSynchronizer(this.getDataSource("FileDatabase"), new ExtensionEnumTableSynchronizer());
		this.index = builder.buildAndDispose();
	}

	@Produces
	public EnumKeyIndex<UUID> produceEnumKeyIndex() {
		return this.index;
	}

}
```